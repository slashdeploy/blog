DOCKER_IMAGE:=tmp/image
RUBY_IMAGE:=$(shell head -n 1 Dockerfile | cut -d ' ' -f 2)
GIT_COMMIT:=$(shell git rev-parse --short HEAD)
BUILD_ENV:=-e JEKYLL_ENV=production

.DEFAULT_GOAL:=dist

.PHONY: check
check:
	jq --version
	aws --version
	docker --version
	bats --version

Gemfile.lock: Gemfile
	docker run --rm -it -v $(PWD):/data -w /data $(RUBY_IMAGE) \
		bundle package

$(DOCKER_IMAGE): Dockerfile Gemfile.lock
	docker build -t slashdeploy/blog .
	mkdir -p $(@D)
	touch $@

.PHONY: init
init: $(DOCKER_IMAGE)
	docker run --rm -it -v $(PWD):/data -w /data slashdeploy/blog \
		bundle exec jekyll new src

.PHONY: dist
dist: $(DOCKER_IMAGE)
	docker run --rm slashdeploy/blog \
		ruby test/seo_test.rb src/_posts
	mkdir -p dist tmp
	docker create -i $(BUILD_ENV) slashdeploy/blog \
		bundle exec jekyll build -d /data -s /usr/src/app/src > tmp/dist_container
	docker start -a $$(cat tmp/dist_container)
	@docker cp $$(cat tmp/dist_container):/data - | tar xf - -C dist --strip-components=1 > /dev/null
	@docker stop $$(cat tmp/dist_container) > /dev/null
	@docker rm -v $$(cat tmp/dist_container) > /dev/null
	@rm -rf tmp/dist_container
	@mkdir -p dist/_sentinel
	@echo "$(GIT_COMMIT)" > dist/_sentinel/$(GIT_COMMIT).txt

.PHONY: test-dist
test-dist:
	env DIST_PATH=$(PWD)/dist SENTINEL_VALUE=$(GIT_COMMIT) test/dist_test.bats
	docker run --rm -v $(CURDIR)/dist:/data slashdeploy/blog \
		bundle exec htmlproofer --internal-domains blog.slashdeploy.com /data 

.PHONY: test-shellcheck
test-shellcheck:
	docker run --rm -v $(PWD):/data:ro -w /data jrotter/shellcheck \
		shellcheck -s bash \
		$(shell find bin script -type f -exec test -x {} \; -print | paste -s -d ' ' -)

.PHONY: test-blog
test-blog:
	bin/blog validate

.PHONY: test-ci
test-ci:
	$(MAKE) test-shellcheck
	$(MAKE) test-dist
	$(MAKE) test-blog

.PHONY: clean
clean:
	rm -rf $(DOCKER_IMAGE) dist
