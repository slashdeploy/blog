module Jekyll
	class Revision < Liquid::Block
		def render(context)
			site = context.registers[:site]
			converter = site.find_converter_instance(::Jekyll::Converters::Markdown)
			output = converter.convert(super(context))

			"<div class='revision-warning'>#{output}</div>"
		end
	end
end

Liquid::Template.register_tag('revision', Jekyll::Revision)
