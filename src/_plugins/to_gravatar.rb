require 'digest/md5'

module Jekyll
  module GravatarFilter
    def to_gravatar(email)
      "//www.gravatar.com/avatar/#{Digest::MD5.hexdigest(email.to_s.strip.downcase.strip)}"
    end
  end
end

Liquid::Template.register_filter(Jekyll::GravatarFilter)
