---
title: "Slashdeploy Newsletter Announcement"
layout: post
author: ahawkins
seo:
  type: BlogPosting
description: >
  A new weekly newsletter on building, deploying, and running software.
keywords:
  - DevOps
  - Newsletter
---

I'm happy to announce the new Slashdeploy weekly newsletter! The
newsletter features field notes, links, and access to special content
and offers. The first edition arrives in two weeks. Subscribe below.

The field notes section is the best part and sets the Slashdeploy
newsletter apart from other weekly link aggregators. Each edition
includes a short (and some are much longer essays!) comment on my
up-to-date experience building, deploying, and running software. This
is "news from the front line" so to speak--lessons and insight from my
daily work. Think of it as DevOps conciousness live stream. Thoughts
and insights are shared here before turning into blog posts of other
content, that is if they even make it that far. The field notes also
set the theme for each edition, so expect to find links relevant to
the field notes.

The newsletter is another channel to help you improve at building,
deploying, and running software. Also I _crave_ conversations,
feedback, and your perspective. Don't hesitate to hit reply or tweet
me with your thoughts. See you in your inbox!
