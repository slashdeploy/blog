---
title: "Introducing await"
layout: post
author: ahawkins
seo:
  type: BlogPosting
description: >
  Introducing the await CLI primarily designed for Kubernetes init
  containers.
keywords:
  - Kubernetes
  - Docker
  - Containers
  - helm
---

I'm leading a migration to [Kubernetes][k8s] at my fulltime job at
Saltside. Our goal is to deploy our SoA application as a [helm][]
chart. This posed a few initial problems. The biggest problem is that
services depend on other services (this is SoA after all).  Service A
may depend on service B to start. Databases are the second problem.
Services cannot start if they cannot connect to their datastores.
These problems occour because pods are started in any order. We can
easily solve this problem with Kubernetes [init
containers][]--specifically awaiting for dependencies to start.

We've open sourced our [await][source] CLI to address these problems.
The `await` command is primarily designed for Kubernetes init
containers, but there are probably other uses cases where it could be
useful. We created `await` because we wanted something that:

* Retried connections repeatedly until a timeout was reached
* Supported all our datastores / service types
* Provided a uniform UX

There didn't seem to be any existing FOSS so we created our own. The
result is a simple shell script with a mix of python built on Alpine
Linux. The resulting is a small Docker image. It shouldn't slow down
pod creation and is small enough to use a base image.

`await` supports the following checks:

* **HTTP Servers**: runs `curl`
* **Redis**: runs `redis-cli ping`
* **MongoDB**: runs `showDBs()`
* **Memcached**: runs `INFO`
* **Local DynamodB**: `ListTables` against a local [Local DynamoDB][]
* **MySQL**: runs `show tables;`
* Arbitrary commands

`await` uses URIS to determine the check
method. Each protocol in the URI maps to a check function. Here are
some examples:

```
# wait for an HTTP server
await http://example.com

# wait for an HTTP server for 2 minutes (default: 60 seconds)
await -r 120 http://example.com

# wait for an HTTP server with extra curl options
await http://example.com -- -H 'Special-Header: Foo'

# wait for a redis server
await redis://redis.mycompany.com

# wait for EC2 access
await cmd -- aws ec2 describe-instances
```

You can pull from `saltside/await` on [docker hub][].

All `await` commands accept a `-r` for retries. The default is 60
attempts. Commands also accept the `-- [EXTRA_OPTS]` syntax to pass
arguments to the underlying call. This should provide users with
enough flexibity to set connection parameters for all their resources.

We're happy to take PRs to the [source][] on GitHub. Big thanks to
[Terje Larsen][terlar] & [Sitesh Jalan][sjalan] for most of the work.

Good luck our there and happy shipping!

[k8s]: http://kubernetes.io
[helm]: https://helm.sh
[source]: https://github.com/saltside/await
[terlar]: https://github.com/terlar
[sjalan]: https://github.com/sjalan
[local dynamodb]: http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html
[docker hub]: https://hub.docker.com/r/saltside/await/
[init containers]: https://kubernetes.io/docs/user-guide/pods/init-container/
