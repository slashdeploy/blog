---
title: "Announcing the Rack Bootcamp"
layout: post
author: ahawkins
seo:
  type: BlogPosting
description: >
  The bootcamp's purpose to save your time by providing the highest
  value information in one place. This way you don't need to spend time
  scraping around the internet for tutorials or screencasts.
keywords:
  - ruby
  - rack
---

I'm happy to share that I've completed my work on the Rack Bootcamp!
The Rack Bootcamp is a fast paced ebook for Ruby programmers eager to
understand the abstractions their web applications build on.

I wrote the Rack Bootcamp to address gaps in the documentation and
blogosphere. The gaps became apparent to me when I started teaching
and mentoring new engineers at my full time job. We do Ruby (proudly)
differently than most places. We are light on web frameworks and rely
heavily on Rack middleware and leveraging different abstraction
layers. We use Sinatra a lot, but everything is all about Rack. This
is where the Rack Bootcamp comes in. Also, I got tired of talking
about this stuff over and over again!

The Rack Bootcamp takes about 30 minutes. Here's what covered:

1. Dead Simple Rack Applications
1. Env
1. Abstractions
1. Middleware
1. Middleware Stacks
1. Rackup
1. Rails & Rack
1. Testing (_Naturally my favorite section!_)
1. Servers

The bootcamp's purpose to save your time by providing the highest
value information in one place. This way you don't need to spend time
scraping around the internet for tutorials or screencasts.

There are three price points. The first package contains the book
itself. The second package includes the book and a one-on-one pairing
session with myself. You can take it as an expert walk through or we
can pair on whatever you need. The third package includes the ebook
and a pairing session with your entire team. This package is aimed at
team leads looking to level up groups of people at once. You can take
it as a group pairing session, general QA, or help hacking on some
code your team is having problems with.

It's launching soon (yay!). Subscribe to the mailing list below and
you'll receive a discount code on launch day.

Good luck out and happy shipping!
