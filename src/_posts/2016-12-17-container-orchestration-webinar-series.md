---
title: "Kubernetes Webinar Series"
layout: post
author: ahawkins
seo:
  type: BlogPosting
description: >
  Outline and information for upcoming 2 part webinar series
  on container orchestration. Part 1 covers the concepts with a hands
  on demo of Kubernetes. Part 2 focuses on the ecosystem and
  production operation aspects.
keywords:
  - CloudAcademy
  - Webinar
  - Docker
  - Containers
  - docker compose
  - kubernetes
  - mesos
  - docker swarm
---

I'm happy to announce a three part webinar series on Docker for
[CloudAcademy][]. The series is a compliment my recent [Docker
course][] that fills the gap in the available introductory material.
The series is a fantastic place to start if you've heard of Docker
(and containers) and want to go to production. Container orchestration
is an important role of any production container system. This series
shines light a key player in the space: Kubernetes.

## Part 1: Hands on Kubernetes

Part 1 covers the high level behind container orchestration and what
differentiates Kubernetes between other tools. This is a hands on
session which covers deploying and scaling a microservice application
to Kubernetes. This is your bootstrapping guide.

## Part 2: Ecosystem & Production Operations

Part 2 is for those of you who may have already used Kubernetes in
production. Kubernetes also supports a robust ecosystem of supporting
tools and projects. Real world systems tend to leverage many different
tools. This session sheds light into these tool and other real world
production systems.

Be sure to check the [webinars][] page for scheduling info. Hope to
see you there.

Good luck our there and happy shipping!

[webinars]: https://cloudacademy.com/webinars/
[cloudacademy]: https://cloudacademy.com
[docker course]: https://cloudacademy.com/cloud-computing/introduction-to-docker-course/
