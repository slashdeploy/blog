---
title: "Benchmarking AWS Cost"
layout: post
author: ahawkins
seo:
  type: BlogPosting
description: >
  Thoughts on benchmarking infrastructure/operational cost and
  relating it other teams/organizations.
keywords:
  - AWS
  - cost
---

[Saltside][] is going through a cost renaissance in the past 6 months.
My team has reduced our AWS costs significantly. Now we're looking
outward to gauge our costs against other teams/organizations. I
understand that is effectively impossible to compare two organizations
because there are too many factors. My approach here is to compare
similar architectures and other practices.

I've put together a questionnaire for discussion with other engineers.

- How do you budget & track your infrastructure cost?
- What's your strategy for reserved capacity / spot instances?
- Are you using containers?
- How do you run you containers? Are you using ECS, Kubernetes, DCOS,
  Docker Swarm, or something else?
- How much do you spend on the horizontally scaleable application
  tier?
- How much do you spend on the data tier? (RDMS, NoSQL, cached,
  key-value stores etc)
- What's the most expensive part of your infrastructure?
- What's your budget for logging, metrics, and alerting?
- How do you measure your infrastructure's efficiency? Example KPIs:
  cost per user, cost per request, percentage of revenue.
- Where and how do you run your pre-production environments?
- Have you considered moving to another cloud provider?
- What do your run yourself vs delegating to AWS?

I'm looking for anyone who will answer this questionnaire and talk
openly about their costs with me. I'll share all our numbers and bits.
Hopefully the conversation is mutually beneficial--if not at least
interesting.

Ping me if you're open to discussing or if you have a better approach
to this.

[saltside]: http://saltside.se
