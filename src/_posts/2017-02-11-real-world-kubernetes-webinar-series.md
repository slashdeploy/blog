---
title: "Real World Kubernetes Webinar Series"
layout: post
author: ahawkins
seo:
  type: BlogPosting
description: >
  Outline and information for upcoming 2 part webinar series
  on Kubernetes tools for cluster ops and application deployment.
keywords:
  - cloudAcademy
  - webinar
  - docker
  - containers
  - kubernetes
  - helm
  - kops
  - cluster ops
---

The [previous webinar series][] was a success, so it's time for more
Kubernetes!  I'm following it up with a second series for
[CloudAcademy][] on two tools in the Kubernetes toolbox: [Helm][] and
[Kops][]. They round out the Kubernetes toolchain from cluster ops all
the way up to application deployment. I'm excited to share these tools
with you to better prepare for your Kubernetes in the real world.

## Part 1: Deploying Applications with Helm

[Helm][] is the Kubernetes package manager. Helm recently graduated
from the Kubernetes incubator program and is now an official
Kubernetes project. This sessions covers what Helm is, and how you can
use for continuous deployment of Kubernetes applications. The session
includes a short presentation on Helm and charts, followed by
technical demonstrations.

## Part 2: Cluster Ops with Kops

[Kops][] is `kubectl` for Kubernetes clusters. You need to create a
Kubernetes cluster somehow. Kops is "the tool" for provisioning and
maintaining clusters on AWS and GCP. This session includes a
presentation on on what Kops is, how it works, and what it can do
followed by technical demos.

Both sessions include QA time. Summary blog posts are posted
afterwards for those who could not attend the sessions.

Be sure to check the [webinars][] page for scheduling info. Hope to
see you there.

Good luck our there and happy shipping!

[previous webinar series]: {% post_url 2016-12-17-container-orchestration-webinar-series %}
[helm]: https://helm.sh
[kops]: https://github.com/kubernetes/kops
[webinars]: https://cloudacademy.com/webinars/
[cloudacademy]: https://cloudacademy.com
