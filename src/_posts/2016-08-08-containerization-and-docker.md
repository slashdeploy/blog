---
title: Containerization & Docker Video
layout: post
author: ahawkins
seo:
  type: BlogPosting
description: 5 minute video describing container techologies & Docker.
keywords:
  - docker
  - containers
  - container techonlogies
  - containerization
  - lxc
---

I put together a short video explaining containerization technologies
and Docker. Check the video out to see how Docker relates.

<iframe src="https://player.vimeo.com/video/177336724" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

_P.S. This is my first stab at video presentations. Look for more in
the future._
