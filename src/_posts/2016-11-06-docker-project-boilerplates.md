---
title: Docker Project Boilerplates
layout: post
author: ahawkins
seo:
  type: BlogPosting
  links:
    - https://www.reddit.com/r/docker/comments/5bekot/docker_projects_boilerplates_ruby_nodejs/
    - https://www.reddit.com/r/node/comments/5belus/docker_project_boilerplate_tdd_smoke_tests/
    - https://www.reddit.com/r/ruby/comments/5bekqo/docker_projects_boilerplates_ruby_nodejs/
description: "A set of project boilerplate templates for Docker projects using Ruby, Node.js, and others."
keywords:
  - docker
  - boilerplate
  - ruby
  - node
  - node.js
---

I'm happy to share my (updated) [Docker project boilerplate][link]
templates for greenfielding new projects. These are updated versions
of the previous [Ruby][] and [Node][] boilerplates. I plan to maintain
these and add new languages as my experience grows. Hopefully I can
add Python, Go, and Java ones soon enough. Pull requests accepted as
always.

The boilerplates are built on `make`, `docker`, and `docker-compose`.
Each boilerplate includes:

* Docker based dependency management
* TDD setup
* Code linting
* An [editorconfig][] file configured for best practices
* Dockerfile configured to run a web server
* Blackbox & whitebox tests for the web server
* `make test-ci`: Run a full battery of tests
* `make test-smoke`: Run a smoke test against production process

They generally follow the same structure. `docker` is used to generate
dependencies so they can be committed to source control. This removes
upstream dependencies on upstream package hosts at build time.
`docker-compose` manages containers for various other targets. One
container runs the process (the boilerplates use a simple web
server as a common case). Another container is used for quick tests
(for interpreted languages which don't require a separate compile
step). Another container is used to smoke test the application
container.

I hope these [boilerplates][link] help you bootstrap new Docker
project.

Good luck out there and happy shipping!

[link]: http://slashdeploy.com/boilerplate.html
[editorconfig]: http://editorconfig.org
[ruby]: {% post_url 2016-05-02-docker_and_ruby_for_tdd_and_deployment %}
[node]: {% post_url 2016-05-18-docker-node-boilerplate %}
