---
title: "Docker Webinar Series: From Dev to Production"
layout: post
author: ahawkins
seo:
  type: BlogPosting
description: >
  Outline and information for upcoming 3 part webinar series
  on Docker. Part 1 covers the cpncept, part 2 covers production
  deployments, and part 3 is open QA forum.
keywords:
  - CloudAcademy
  - Webinar
  - Docker
  - Containers
  - docker compose
  - docker machine
  - kubernetes
  - mesos
  - docker swarm
---

I'm happy to announce a three part webinar series on Docker for
[CloudAcademy][]. The series is a compliment my recent [Docker
course][] that fills the gap in the available introductory material.
The series is a fantastic place to start if you've heard of Docker and
want to learn how to build, ship, and deploy Docker applications. I'm
especially looking forward to the production focused part 2, and the
larger Q/A wrap in part 3.

Here's the abstract for entire series:

> Introduce Docker and demonstrate applications to the development
> process and going all the way to production. Part 1 one is a brief
> introduction to Docker with focus on use cases. Part 2 covers
> production deployment options. Part 3 wraps it up with a summary and
> large question/answer section. There is question/answer at the end
> of each session.

Also here is the description for each part. The content for part 1 and
2 may change based on feedback.

## Part 1: From Idea to Dev

_30 minute session with ~5-10 minutes Q/A_.

session is intended for those who may have heard of Docker, but
have not actively worked with or applied Docker. The goal for the
session to clarify your understanding of what Docker is and isn't.
Then demonstrate tools and techniques you can start to use right now.
Next we introduce production orchestration ideas covered in Part 2.
The session ends with some time for questions and answers.

[Click here][signup] to sign up for part 1.

## Part 2: From Dev to Production

_45 minute session with ~10-15 minutes Q/A_.

Part 1 took Docker from an abstract idea to a concrete solution.  Part
2 takes that solution into the hands of your users/customers. This
session gives an overview of different deployment options with a focus
on larger multi container applications via orchestration tools.
Smaller scale applications are considered as well. The session
includes with a demo of deploying a multi stage Docker application
with docker-compose and docker machine. The session ends with some
time for questions and answers.

_The `docker-machine` deployment pipeline demo didn't make it into my
course, so I'm looking forward to sharing it here. This is may
favorite session because it's more production/deployment focused._

This session is planned for November. The date will be announced on
CloudAcademy's [webinar page][webinars].

## Part 3: Production & Beyond

_15 minute session with ~30 minutes Q/A_.

Part 2 covered putting application's in production. This session
covers what happens next and other larger trends in the container
ecosystem. The session is more discussion focused to cover an
questions from previous parts.

This session is planned for December. The date will be announced on
CloudAcademy's [webinar page][webinars].

I'm looking forward to seeing you all in the webinar and answering
your questions. Good luck out there and happy shipping!

[webinars]: https://cloudacademy.com/webinars/
[cloudacademy]: https://cloudacademy.com
[docker course]: https://cloudacademy.com/cloud-computing/introduction-to-docker-course/
[signup]: https://attendee.gotowebinar.com/register/7032500938455855107
