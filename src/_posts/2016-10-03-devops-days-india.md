---
title: "Devops Days India"
layout: post
author: ahawkins
seo:
  type: BlogPosting
description: >
  It was summer 2014. Docker just hit 1.0. The engineering team was
  given a rare chance to rewrite the product from scratch. We
  immediately moved to Docker from development and production. We
  build our own orchestration tool to fit our unique requirements.
  Things have changed a lot since 2016. Come with me and join for a 2
  year wide retrospective on how we succeeded, failed, what we
  learned, and how to better production Docker users in 2016.
keywords:
  - Devops Days
  - Devops
  - Docker
  - Devops Days India
  - Bangalore
---

I'm happy to announce that I'm speaking at [Devops Days India][link]!
The topic is near dear to my heart: build and shipping code! Here's
the talk description:

> It was summer 2014. Docker just hit 1.0. The engineering team was
> given a rare chance to rewrite the product from scratch. We
> immediately moved to Docker from development and production. We
> build our own orchestration tool to fit our unique requirements.
> Things have changed a lot since 2016. Come with me and join for a 2
> year wide retrospective on how we succeeded, failed, what we
> learned, and how to better production Docker users in 2016.

Please get some tickets and join me. I'm looking forward to meeting
you all.

[link]: http://devopsdaysindia.org/
