require 'bundler/setup'

require 'minitest/autorun'
require 'yaml'

class SEOTest < MiniTest::Test
  attr_reader :posts

  def setup
    @posts = Dir["#{ARGV[0]}/*.md"]
  end

  def test_posts_have_descriptions
    posts.each do |post|
      data = YAML.load_file post
      assert data.key?('description'), "#{post} missing description"
    end
  end

  def test_posts_have_keywords
    posts.each do |post|
      data = YAML.load_file post
      assert data.key?('keywords'), "#{post} is missing keywords"
      assert_instance_of Array, data.fetch('keywords'), "#{post} keywords incorrect"
    end
  end

  def test_posts_have_seo_type
    posts.each do |post|
      data = YAML.load_file post
      assert_equal 'BlogPosting',  data.dig('seo', 'type'), "#{post} missing description"
    end
  end

  def test_posts_have_seo_links_are_valid_uris
    posts.each do |post|
      data = YAML.load_file post
      seo = data.fetch('seo')

      next unless seo.key?('links')

      seo.fetch('links').each do |link|
        assert URI(link), "#{link} is invalid"
      end
    end
  end
end
